package responder

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// Description :
//		This function is a short hand for sending a http 500 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondInternalServerError(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusInternalServerError, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 501 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondNotImplemented(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusNotImplemented, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 503 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondServiceUnavailable(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusServiceUnavailable, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 507 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondInsufficientStorage(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusInsufficientStorage, nil, message...)
}
