package responder

import (
	"fmt"
	"time"

	"github.com/labstack/echo/v4"
)

type responseTemplate struct {
	Code    int           `json:"Code"`
	Message string        `json:"Message"`
	Payload interface{}   `json:"payload"`
	Delay   time.Duration `json:"delay,omitempty"`
}

func Respond(c echo.Context, status int, payload interface{}, message ...interface{}) error {
	return c.JSON(status, responseTemplate{
		Code:    status,
		Message: fmt.Sprint(message),
		Payload: payload,
	})
}
