package responder

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// Description :
//		This function is a short hand for sending a http 400 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondBadRequest(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusBadRequest, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 401 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondUnauthorized(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusBadRequest, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 403 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondForbidden(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusForbidden, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 404 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondNotFound(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusForbidden, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 405 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondMethodNotAllowed(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusMethodNotAllowed, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 406 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondNotAcceptable(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusNotAcceptable, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 408 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondRequestTimeout(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusRequestTimeout, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 408 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondConflict(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusConflict, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 411 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondLengthRequired(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusLengthRequired, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 413 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondRequestEntityTooLarge(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusRequestEntityTooLarge, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 415 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondUnsupportedMediaType(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusUnsupportedMediaType, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 416 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondRequestedRangeNotSatisfiable(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusRequestedRangeNotSatisfiable, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 418 response
// Parameters :
//      c echo.Context : Required
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} : Optional
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondTeapot(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusTeapot, nil, message...)
}
