package responder

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// Description :
//		This function is a short hand for sending a http 200 response
// Parameters :
//      c echo.Context :
//          The current request context from echo. Must never be set to nil.
//		body interface{} :
//			The body to send as response
//		message ...interface{} :
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondOk(c echo.Context, body interface{}, message ...interface{}) error {
	return Respond(c, http.StatusOK, body, message...)
}

// Description :
//		This function is a short hand for sending a http 201 response
// Parameters :
//      c echo.Context :
//          The current request context from echo. Must never be set to nil.
//		body interface{} :
//			Must be the created ressource.
//		message ...interface{} :
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondCreated(c echo.Context, body interface{}, message ...interface{}) error {
	return Respond(c, http.StatusCreated, body, message...)
}

// Description :
//		This function is a short hand for sending a http 202 response
// Parameters :
//      c echo.Context :
//          The current request context from echo. Must never be set to nil.
//		body interface{} :
//			The body to send as response if there must be one.
//		message ...interface{} :
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondAccepted(c echo.Context, body interface{}, message ...interface{}) error {
	return Respond(c, http.StatusAccepted, body, message...)
}

// Description :
//		This function is a short hand for sending a http 204 response
// Parameters :
//      c echo.Context :
//          The current request context from echo. Must never be set to nil.
//		message ...interface{} :
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func RespondNoContent(c echo.Context, message ...interface{}) error {
	return Respond(c, http.StatusNoContent, nil, message...)
}

// Description :
//		This function is a short hand for sending a http 205 response
// Parameters :
//      c echo.Context :
//          The current request context from echo. Must never be set to nil.
//		body interface{} :
//			The new ressource value
//		message ...interface{} :
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func ReturnResetContent(c echo.Context, body interface{}, message ...interface{}) error {
	return Respond(c, http.StatusResetContent, body, message...)
}

// Description :
//		This function is a short hand for sending a http 206 response
// Parameters :
//      c echo.Context :
//          The current request context from echo. Must never be set to nil.
//		body interface{} :
//			The part of the response
//		message ...interface{} :
//			Informations about the response. Used to fill the message field of the response template.
// Returns :
// 		error :
// 			The encountered error if there has one. Nil otherwise
func ReturnPartialContent(c echo.Context, body interface{}, message ...interface{}) error {
	return Respond(c, http.StatusPartialContent, body, message...)
}
