module gitlab.com/atelias-photos/tools

go 1.13

require (
	github.com/google/uuid v1.2.0
	github.com/labstack/echo/v4 v4.2.0
)
